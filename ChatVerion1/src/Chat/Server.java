package Chat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/* C/S 客户端/服务端
 * B/S 客户端统一的结构
 * 
 * 服务端应用程序
 * 
 */
public class Server {
	// ServerSocket 运行在服务端的Socket
	private ServerSocket server;
	//保存所有客户端输出流的集合，用于共享以便广播
	private List<PrintWriter> allOut;
	//线程池
	private ExecutorService threadPool;
	
	/*
	 * 构造方法，用于初始化服务端
	 */
	public Server() {
		try {
			server = new ServerSocket(8088);// 申请一个不重复的端口
			allOut=new ArrayList<PrintWriter>();
			threadPool=Executors.newFixedThreadPool(40);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/*
	 * 启动服务端的方法
	 */
	public void start() {

		try {
		
			// 监听8082端口，等待客户端的连续
			/**
			 * accept方法是一个阻塞方法，直到一个客户端连接，该方法就会返回针对该客户端的Socket
			 * 也就是说，通过这个Socket就可以与连接上的客户端进行交互了
			 */
		   while(true){
			 
			System.out.println("等待客户端连接...");
			Socket socket = server.accept();
			System.out.println("客户端连接上了。");
			
			/**
			 * 当一个客户端连接后，
			 * 启动一个线程（ClientHander），将连接的客户端的Socket传入
			 */
			ClientHander hander=new ClientHander(socket);
			//创建线程，指派任务并启动
		//	Thread thread =new Thread(hander);
		//	thread.start();
			
			//将任务交给线程池
			threadPool.execute(hander);
			}

			
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
	
	/*
	 * 向共享集合中添加一个客户的输出流
	 * 
	 * synchronized 既是同步锁又是互斥锁
	 * 同步：方法内部
	 * 互斥：方法之间
	 * 因为它控制的是统一个对象
	 */
	public synchronized void addOut(PrintWriter  pw){
		allOut.add(pw);
	}
	/*
	 * 向给定的客户端的输出流从共享集合中删掉
	 */
	public synchronized void removeOut(PrintWriter  pw){
		allOut.remove(pw);
	}
	/*
	 * 将给定的消息转发给所有客户端发送消息
	 */
	public synchronized void sendMessageToAllClient(String message){
		for(PrintWriter pw :allOut){
			pw.println(message);
		}
	}
	
	
	
	
	public static void main(String[] args) {
		Server server = new Server();
		server.start();

	}
	/**
	 * Server用于处理不同客户端的多线程任务，
	 * 该类主要负责与给定的客户端进行交互
	 * @author soft01
	 *
	 */
	class ClientHander implements Runnable{
		 //用于交互的客户端Socket
		private Socket socket;
		/*
		 * 构造该线程任务的同时要求指派需要交互的
		 * 客户端的socket
		 */
		public ClientHander(Socket socket){
			this.socket=socket;
		}
		public void run() {
			//定义在try外面，保证finally可以关闭它
			PrintWriter pw=null;
			try{
				/*
				 * 线程启动后，我们首先获取该客户端的输出
				 * 流，将其放入共享集合
				 */
				OutputStream out=socket.getOutputStream();
				OutputStreamWriter osw=new OutputStreamWriter(out,"UTF-8");
			    pw=new PrintWriter(osw,true);
			   //将该客户端的输出流存入共享集合
				addOut(pw);
				
				/*
				 * 输出当前在线人数
				 * 思路：保存所有输出流的集合中有多少元数就说明有多少个用户
				 * 所以我们直接输出集合的元数即可
				 */
				System.out.println("当前在线人数："+allOut.size());
				
				
				/**
				 * 通过Socket可以获取自己（服务器） 以及对方（连接上的客户端）的相关信息 例如地址及端口号
				 */
				// 获取自己的信息
			/* InetAddress address = socket.getLocalAddress();// InetAddress里封装了各种信息
				// 输出自己的端口号
				System.out.println("本机端号：" + address.getHostName());
				// 输出自己的ip
				System.out.println("本机ip：" + address.getHostAddress());
				// 输出自己地址的完全限定名
				System.out.println("本机完全限定名：" + address.getCanonicalHostName());*/

				// 获取远端的信息
				InetAddress inet = socket.getInetAddress();
				System.out.println("客户端端口：" + socket.getPort());
				System.out.println("客户端完全限定名：" + inet.getCanonicalHostName());
				System.out.println("客户端ip地址：" + inet.getHostAddress());

				/*
				 * 服务器这边我们通过该客户端的Socket 获取输入流，来读取客户端发送过来的信息
				 */
				InputStream in = socket.getInputStream();
				InputStreamReader isr = new InputStreamReader(in, "UTF-8");
				 BufferedReader br = new BufferedReader(isr);
			
				
				/*
				 * 服务器这边我们通过该客户端的Socket 获取输出流，来向客户端发送信息
				 */
				/*OutputStream os=socket.getOutputStream();
				OutputStreamWriter osw=new OutputStreamWriter(os,"UTF-8");
				final PrintWriter pw=new PrintWriter(osw,true);
				Thread t2=new Thread(){
					Scanner  sc=new Scanner(System.in);
					public void run(){
						String input=null;
						while(true){
							System.out.println("请输入");
							input=sc.nextLine();
							pw.println(input);
						}
						
					}
				};*/

				//Thread t1 = new Thread() {
				//	public void run() {
				
				/*
				 * windows 与linux的差异：对于客户端主动断开连接后windows通过输入流读取数据时会抛出异常
				 * br.readLine（）抛出异常
				 * linux则同过输入流读取数据时获取null
				 * br.readLine()返回值为null 
				 * 对于缓冲字符输入流的readLine方法
				 * 若返回值为null，表示任何数据都可读了；
				 */
						String line=null;
						/*
						 * 第一次读取到的用户输入信息是
						 * 该用户的昵称
						 */
						String nickName=br.readLine();
						
						while ((line=br.readLine())!=null) {
							
							/*String message;
							try {
								message = br.readLine();
								if (message == null) {
									break;
								}
								System.out.println(inet.getHostAddress()+"客户端说：" + message);
							} catch (IOException e) {

								e.printStackTrace();
							}*/
							/**
							 *每当读取到客户端发送过来的消息后就广播给所有的客户
							 */
						sendMessageToAllClient(nickName+"说："+ line);
							
							

						}
					//}
				//};
				
				//t1.start();
				//t2.start();		
				
				
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				
				/*
				 * 无论是谁是否正常交互完毕，
				 * 最终都要于该客户端断开连接
				 */
				try{
					/*
					 * 首先当服务端要与客户端断开连接前，先将该客户端的输出流从共享集合
					 * 中删掉，这样，后续的消息就不用转发给该客户
					 */
					removeOut(pw);
					/*
					 * 当一个客户端下线后，同样输出当前人数
					 */
					System.out.println("当前在线人数："+allOut.size());
					
					//关闭Soket那么通过该Socket获取的输入与输出流就全部关闭了
					pw.close();
					
					socket.close();
				}catch(IOException e){
					//e.printStackTrace();
				}
			}
		}}
}
