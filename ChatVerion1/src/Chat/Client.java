package Chat;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Properties;
import java.util.Scanner;

/**
 * 客户端应用程序
 * 
 * @author soft01
 * 
 */
public class Client {
	// 运行在客户端的Socket，使用它来连接到服务器端
	private Socket socket;
	

	public Client() {
		try {
			/*
			 * 读取配置文件
			 * clientconfig.properties
			 */
		Properties	properties=new Properties();
		/*
		 * load(InputStream in)
		 * 该方法用于从给定的流中读取数据，并解析
		 */
		FileInputStream fis=new FileInputStream("clientconfig.properties");
		properties.load(fis);
		//获取服务器端的地址
		String host=properties.getProperty("server_host");
		String port=properties.getProperty("server_port");
			
			/*
			 * 创建Socket的同时即new的时候就发起了连接 连接不上，就会抛出异常 连接上就创建socket
			 */
			socket = new Socket(host, Integer.parseInt(port));// ip:127.0.0.1=localhost	
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * 客户端的启动方法
	 */
	public void start() {
		try {
			/*
			 * 客户端启动后我们先启动线程来接受服务器端发送过来的信息
			 */
			GetMessageHander hander=new GetMessageHander();
			Thread t=new Thread(hander);
			/*
			 * 将获取服务器消息的线程设置为后台线程，这样当我们停止了向服务器端发送信息的线程
			 * 后，该线程会一同结束。
			 */
			t.setDaemon(true);
			t.start();
			
			/**
			 * 获取用于与服务端发送信息的输出流
			 */
			OutputStream out = socket.getOutputStream();
			OutputStreamWriter osw = new OutputStreamWriter(out, "UTF-8");
			final PrintWriter pw = new PrintWriter(osw, true);
			/*
			 * 用于读取服务器端发送的信息
			 */
		/*	InputStream is = socket.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			final BufferedReader br = new BufferedReader(isr);

			Thread t1 = new Thread() {
				public void run() {
					String line = null;
					while (true) {
						try {
							line = br.readLine();
							if (line == null) {
								break;
							}
							System.out.println("服务器说：" + line);
						} catch (IOException e) {

							e.printStackTrace();
						}

					}
				}
			};*/
			//Thread t2 = new Thread() {
				Scanner sc = new Scanner(System.in);
				//public void run() {
					String input = null;
					System.out.println("请输入昵称：");
					String nickName=sc.nextLine();
					//对昵称做点验证
					//首先把昵称发送到服务器
					pw.println(nickName);
					//提示用户开始聊天
					System.out.println("你好！"+nickName+"，开始聊天吧！");
					
					while (true) {
						//System.out.print("请输入:");
						input = sc.nextLine();
						pw.println(input);
					}
				//	}
				//};
			//	t1.start();
			  //	t2.start();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
	}

	public static void main(String args[]) {
		Client client = new Client();
		client.start();
	}
	/**
	 * 该线程类用于循环读取来自服务器的消息，并输出控制台
	 * @author soft01
	 *
	 */
	class GetMessageHander implements Runnable{

		@Override
		public void run() {
			/*
			 * 1.通过Socket获取输入流
			 * 2.将输入流转换为缓冲字符输入流
			 * 3.循环读取服务端发送的信息并输出到控制台
			 */
			try{
			InputStream	in=socket.getInputStream();
			InputStreamReader isr=new InputStreamReader(in,"utf-8");
			BufferedReader br=new BufferedReader(isr);
			
			//String line=null;
			while(true){
				System.out.println(br.readLine());
			}
			}catch(Exception e){
				
			}
			
		}
		
	}
}
