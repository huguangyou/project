package com.tetris;

import java.awt.image.BufferedImage;
/*
 * 格子属性
 * */
public class Cell {
	private int row;
	private int col;
	private BufferedImage image;
	/* 根据给定的位置，和图片初始化
	 * 当前格子对象的数据
	 * row 指定的位置row
	 * col 指定的位置col
	 * image 图片
	 * */
	public Cell(int row ,int col,BufferedImage image){
		this.col=col;
		this.image=image;
		this.row=row;
	}
	public int getRow() {
		return row;
	}
	public void setRow(int row) {
		this.row = row;
	}
	public int getCol() {
		return col;
	}
	public void setCol(int col) {
		this.col = col;
	}
	public BufferedImage getImage() {
		return image;
	}
	public void setImage(BufferedImage image) {
		this.image = image;
	}
	public String toString(){
		return row+","+col;
	}
    
	void moveRight(){
		col++;//坐移d个格子；
	}
	public void moveLeft() {
		col--;
		
	}
	public void softDrop() {
		row++;
		
	}
	
	
}
