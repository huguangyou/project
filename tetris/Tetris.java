package com.tetris;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;

//import com.tarena.Cell;
//import com.tarena.Tetromino;



public class Tetris extends JPanel {
	public static final int CELL_SIZE=26;
	private int score;//总分；
	private int lines;//销毁行数；
	private Cell[][]wall=new Cell[ROWS][COLS];//尽量不用字面量= =直接量：直接写出的数；
	public final static int ROWS=20;//设置为public没关系，它不可改变
	public final static int COLS=10;

	private Tetromino tetromino;//正在下落的方块：是七种之一
	private Tetromino nextOne;//下一个准备下落的方块
	
	private static int[]scoreTable={0,1,10,100,500};
                    //     lines=   0 1	 2  3   4

   private int index;
   private int level;
   private int speed;
   private int interval=10;//定时器 间隔时间
   private Timer timer;
   
   private int state;
	private static final int GAME_OVER = 2;
	private static final int PAUSE=1;
	private static final int RUNNING=0;
	
	/*背景图片的引用*/
	private static BufferedImage background;
	public static BufferedImage T;
	public static BufferedImage L;
	public static BufferedImage O;
	public static BufferedImage S;
	public static BufferedImage Z;
	public static BufferedImage J;
	public static BufferedImage I;
	public static BufferedImage pause;
	public static BufferedImage gameOver;
	//利用静态代码块将图片读取为内存对象
	static{
		try{
		//要求Tetris类与tetris.png在同一个包中
			//静态方法调用 类名.方法
		background=ImageIO.read(Tetris.class.getResource("TETRIS.png"));
		T=ImageIO.read(Tetris.class.getResource("T.png"));
		S=ImageIO.read(Tetris.class.getResource("S.png"));
		O=ImageIO.read(Tetris.class.getResource("O.png"));
		L=ImageIO.read(Tetris.class.getResource("L.png"));
		I=ImageIO.read(Tetris.class.getResource("I.png"));
		Z=ImageIO.read(Tetris.class.getResource("Z.png"));
		J=ImageIO.read(Tetris.class.getResource("J.png"));
		pause=ImageIO.read(Tetris.class.getResource("pause.png"));
		gameOver=ImageIO.read(Tetris.class.getResource("game-over.png"));
		}catch(Exception e){
			e.printStackTrace();
			}
	}
	
	
	//*******构造器************
	public Tetris(){}
	
	
	
	/** JPanel修改绘制功能，请重写paint（）方法*/
	public void paint(Graphics g){
		//g 是绑定在面板上的画笔
		 g.setColor(Color.black);
	    //g.drawString("Hello  ", 30, 40);
	    //draw绘 paint涂
		 g.drawImage(background, 0, 0, null);
		 g.translate(15, 15);//坐标系平移；
		 
		 
		 
		 paintWall(g);//绘制墙
		 paintTetromino(g);
		 paintNextOne(g);
		 //在paint方法中增加分数
		 paintScore(g);
		 if(state==PAUSE){g.drawImage(pause,-15,-15,null);}
		 if(state==GAME_OVER){g.drawImage(gameOver,-15,-15,null);}
		
	}
	
	
	//************计分数的方法****************************
	private void paintScore(Graphics g) {
		int x=292;
		int y=165;
		//定义字体；
		Font f=new Font(Font.SERIF,Font.BOLD,30);
		g.setFont(f);
		
		//画阴影；
		g.setColor(Color.WHITE);
		g.drawString("SCORE:"+score, x+3, y+3);
		y+=5;
		g.drawString("LINES:"+lines, x+3, y+47);
		g.drawString(" LEVEL:"+level, x+3, y+97);
		//画任意一种颜色
		int c=0x667799;
		g.setColor(new Color(c));
		
		
		g.drawString("SCORE:"+score,x,y);
		y+=50;
		g.drawString("LINES:"+lines, x, y);
		y+=50;
		g.drawString(" LEVEL:"+level, x, y);
	}



	//*********************绘制墙********************
	public void paintWall(Graphics g){
		for(int row=0;row<ROWS;row++){
			for(int col=0;col<COLS;col++){
				Cell c=this.wall[row][col];
				int x=col*CELL_SIZE;
				int y=row*CELL_SIZE;
				if(c==null){
					g.drawRect(x, y, 26, 26);//以x，y为位置画一个长宽为26的矩形
					}else{
						g.drawImage(c.getImage(),x,y,null);
						}
			}
		}
	}
	
	
	
	/*绘制当前（thi）面板上正在下落的方块*/
	private void paintTetromino(Graphics g){
		if(this.tetromino==null){	//任何的输入数据都要做检查,如果有空的话就不画，以免后面出现空指针异常
			return ;
		}
		Cell[] cells=this.tetromino.cells;
		for(int i=0; i<cells.length;i++){
			Cell cell=cells[i];//Cell变量引用了cells【】数组中每个对象（元数）
			int x=cell.getCol()*CELL_SIZE;
			int y=cell.getRow()*CELL_SIZE;
			g.drawImage(cell.getImage(),x,y,null);
		}
	}
	private void paintNextOne(Graphics g){
		//任何的输入数据都要做检查,如果有空的话就不画，以免后面出现空指针异常
		if(this.tetromino==null){	
			return ;
		}
		Cell[] cells=this.nextOne.cells;
		for(int i=0; i<cells.length;i++){
			Cell cell=cells[i];//Cell变量引用了cells【】数组中每个对象（元数）
			int x=(cell.getCol()+10)*CELL_SIZE;
			int y=(cell.getRow()+1)*CELL_SIZE;
			g.drawImage(cell.getImage(),x,y,null);
		}
	}
	
	
	
	/*在tetris中添加启动方法*/
	private void action() {
		//wall[8][5]=new Cell(8,5,T);
		tetromino=Tetromino.randomOne();//初始化 一运行就初始化
		nextOne=Tetromino.randomOne();
		
		//在action方法中添加定时器控制
		timer=new Timer();
		timer.schedule(new TimerTask(){
			public void run(){
				index++;
				level=lines/100+1;
				speed =41-level;
				speed =speed<=0?1:speed;
				if(state==RUNNING&&index%speed==0){
					softDropAction();
				}
				repaint();
			}
		},10,10);
	
		//1.实现键盘监听接口，创建监听对象
		KeyListener l=new KeyListener(){
			public void keyPressed(KeyEvent e) {
				//long time =e.getWhen();
				int key=e.getKeyCode();
				System.out.println("按下了");
		      if( key==KeyEvent.VK_Q){
		    	  System.exit(0);
		    	  }
		      switch(state){
			   case GAME_OVER:
				   if(key==KeyEvent.VK_S){
					   restarAction();
				   }
				   return;
				case PAUSE: 
					if(key==KeyEvent.VK_C){
						state=RUNNING;
						}
					return;
				case RUNNING:
					if(key==KeyEvent.VK_P){
						state=PAUSE;}
					}
			  
			   switch(key){
				case KeyEvent.VK_RIGHT:
					//tetromino.moveRight();//修该数据
					moveRightAction();//Tetris.this.moveRightAction(); Tetris.this就是tetris类的对象
					break;
				case KeyEvent.VK_LEFT:
					moveLeftAction();//Tetris.this.moveLeftAction();
					break;
				case KeyEvent.VK_DOWN:
					softDropAction();
					break;
				case KeyEvent.VK_SPACE:
					hardDropAction();
					break;
				case KeyEvent.VK_UP:
					rotateRightAction();
					break;
				}
			
				
				//是JPanel的方法：使尽快执行paint（）
				repaint();//重绘界面
				}
			
			
			
			public void keyReleased(KeyEvent e) {}
			public void keyTyped(KeyEvent e) {}
		};
		//2.在当前面板注册监听器对象
		this.addKeyListener(l);
		this.setFocusable(true);//为当前面板获取输入焦点，有焦点的才能获得键盘输入的消息
		//3.等待键盘消息，如果有按键按下将执行监听器对象的方法
		this.requestFocus();
		}
	
	//*************游戏重新开始******************
	private void restarAction() {
		this.lines = 0;
		this.score = 0;
		this.wall = new Cell[ROWS][COLS];
		this.tetromino = Tetromino.randomOne();
		this.nextOne = Tetromino.randomOne();
		this.state = RUNNING;
		this.index = 0;
	}
	
	
	public void rotateRightAction() {
		tetromino.rotateRight();
		if(outOfBounds()||coincide()){
			tetromino.rotateLeft();
		}
	}
	
	/*Tetris；类移动右方动作，流程控制方法*/
	public void moveRightAction(){
		//this代表当前对象，就是当前Tetris游戏对象(main 方法中new Tetris)
		this.tetromino.moveRight();
		//this.tetromino.moveLeft();
		if(outOfBounds()||coincide()){
			//outOfBounds（）方法判断当前正在下落的方块是否出界，true表示出界，false没有出界
			this.tetromino.moveLeft();
		}
	}
	public void moveLeftAction(){
		tetromino.moveLeft();
		//coincode: 重合 检查4格方块与墙是否重合
		if(outOfBounds() || coincide()){
			tetromino.moveRight();
		}
	}
	//*******软下落*****************
	public void softDropAction(){
		if(canDrop()){
			tetromino.softDrop();
		}else{
			landIntoWall();
			
			int lines=destroyLines();
			
			this.score+=scoreTable[lines];
			this.lines+=lines;
			
			if(isGameOver()){
				state=GAME_OVER;
			}else{
				tetromino=nextOne;
				nextOne=Tetromino.randomOne();
			}
		}
	} 
	private boolean canDrop(){
		Cell[]cells=tetromino.cells;
		for(int i=0;i<cells.length;i++){
			Cell cell=cells[i];
			int row=cell.getRow();
			if(row==ROWS-1){//到此不能下落
				return false;
			}
			
		}
		for(int i=0;i<cells.length;i++){
			Cell cell=cells[i];
			int row=cell.getRow();
			int col=cell.getCol();
			if(wall[row+1][col]!=null){
				return false;
				}
			}
		return true;
	}

	private boolean isGameOver() {
		Cell[]cells=nextOne.cells;
		for(int i=0;i<cells.length;i++){
			Cell cell=cells[i];
			int row=cell.getRow();
			int col=cell.getCol();
			if(wall[row][col]!=null){
				return true;//游戏结束
			}
		}
		return false;
	}


    //************销毁满行******************
	private int destroyLines() {
		lines=0;
		for(int row=0;row<ROWS;row++){
			if(fullCells(row)){
				deleteRow(row);
			lines++;
			}
		}
		return lines;
	}


	private void deleteRow(int row) {
		for(int i=row;i>=1;i--){
			System.arraycopy(wall[i-1], 0, wall[i], 0, COLS);
		}
		Arrays.fill(wall[0],null);//添加第零行墙，并每个格子置空
	
		
		
	}


 //*******判断是否满行*****************************
	private boolean fullCells(int row) {
		Cell[]line=wall[row];
		for(int i=0;i<line.length;i++){
			Cell cell=line[i];
			if(cell==null){
				return false;//检查每行每个格子是否是满的，不是，就返回
			}
		}
			
		return true;//每行每个格子都是满的就返回满的
	}



	//************进墙*******************
	private void landIntoWall() {
		Cell[]cells=tetromino.cells;
		for(int i=0;i<cells.length;i++){
			Cell cell=cells[i];
			int row=cell.getRow();
			int col=cell.getCol();
			wall[row][col]=cell;
			}
		
	}



	//*************硬下落****************
	public void hardDropAction(){
		while(canDrop()){
			tetromino.softDrop();
		}
		landIntoWall();
		
		int lines = destroyLines();
		this.score+=scoreTable[lines];//0到n都用数组搞定
		this.lines+=lines;
		
		if(isGameOver()){
			state = GAME_OVER;
		}else{
			tetromino = nextOne;
			nextOne = Tetromino.randomOne();
		}
	} 
	
	
	//****检查当前方块是否与墙上的砖块重合
	private boolean coincide(){
		if(this.tetromino==null){
			return false;
		}
		Cell[]cells=this.tetromino.cells;
		for(int i=0;i<cells.length;i++){
			Cell cell=cells[i];
			int row=cell.getRow();
			int col=cell.getCol();
			if(wall[row][col]!=null){
				return true;
			}
		}
		return false;
	}
	
	
	
	//**********检查当前游戏中正在下落的方块是否出界
	private boolean outOfBounds() {
		//任何的输入数据都要做检查,如果有空的话就不画，以免后面出现空指针异常
		if(this.tetromino==null){
			
			return true;
		}
		Cell[]cells=this.tetromino.cells;
		for(int i=0;i<cells.length;i++){
		Cell	cell=cells[i];
		int col=cell.getCol();
		int row=cell.getRow();
		if(col<0||col>=COLS||row<0||row>=ROWS){
			return true;
		}
		}
		return false;
	}
	
	
	
	
	public static void main(String []args){
		JFrame frame=new JFrame();
		//JPanel panel=new JPanel();
		Tetris tetris=new Tetris();
		//tetris.setBackground(Color.black);
		frame.add(tetris);
		frame.setSize(530,580);
		//frame在显示的时候 ，会尽快调用panel的
		//paint方法绘制显示界面效果
		
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);//会自动调用paint（）方法
		tetris.action();
	}
	

}
