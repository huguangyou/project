package com.tetris;

import java.util.Random;



/*
 * 4个方块类
 */
public abstract class Tetromino {
	protected Cell[] cells=new Cell[4];
	//考点：protected Cell[] cells;A编译错误 B运行错误 C输出结果
	//定义任何引用变量首先要想到何时初始化，让它引用对象，否则很有可能出现空指针异常
	
	 protected State[]states;//存储旋转状态的
	 protected int index=10000;//存储旋转序号的
	 
    protected class State{int row0,col0,row1,col1,row2,col2,row3,col3;

	public State(int row0, int col0, int row1, int col1, int row2, int col2,
			int row3, int col3) {
		super();
		this.row0 = row0;
		this.col0 = col0;
		this.row1 = row1;
		this.col1 = col1;
		this.row2 = row2;
		this.col2 = col2;
		this.row3 = row3;
		this.col3 = col3;
	}
    
    }
    /*Tetromino类中添加一个旋转方法*/
    public void rotateRight(){
    	//输入数据cells states
    	index++;
    	int n=index%states.length;//取余可以用与所有的周而复始运算
    	State s=states[n];            //形状的变化 s1—>s2—》s3——》s0——》s1
                                   //         n=1  2    3     
    	Cell o=cells[0];//旋转轴【0】
    	int row=o.getRow();
    	int col=o.getCol();
    	//[1]格子的位置
    	cells[1].setRow(row+s.row1);
    	cells[1].setCol(col+s.col1);
    	//[2]格子位置
    	cells[2].setRow(row+s.row2);
    	cells[2].setCol(col+s.col2);
    	//【3】格子的位置
    	cells[3].setRow(row+s.row3);
    	cells[3].setCol(col+s.col3);
    }
    public void rotateLeft(){
    	//输入数据cells states
    	index--;
    	int n=index%states.length;//取余可以用与所有的周而复始运算
    	State s=states[n];            //形状的变化 s1—>s2—》s3——》s0——》s1
                                   //         n=1  2    3     
    	Cell o=cells[0];//旋转轴【0】
    	int row=o.getRow();
    	int col=o.getCol();
    	//[1]格子的位置
    	cells[1].setRow(row+s.row1);
    	cells[1].setCol(col+s.col1);
    	//[2]格子位置
    	cells[2].setRow(row+s.row2);
    	cells[2].setCol(col+s.col2);
    	//【3】格子的位置
    	cells[3].setRow(row+s.row3);
    	cells[3].setCol(col+s.col3);
    }
    
    
    
    
    
	//随机生成某个类型的方块 注意：随机（Random）和某一个方块（确定返回值为父类）
	//调用这个方法，将创建新对象，简单工厂方法（面试点）
	public static Tetromino randomOne(){
    	Random r=new Random();
    	int type=r.nextInt(7);//[0,7)
    	switch(type){
    	case 0:return new T();
    	
    	case 1:return new S();
    	
    	case 2:return new I();
    	
    	case 3:return new L();
    	
    	case 4:return new J();
    	
    	case 5:return new Z();
    	
    	case 6:return new O();
    	}
    	return null;//解决编译时没有返回值的错误，但运行时不会执行该步骤
    }

	public void moveRight(){
		for(int i=0;i<cells.length;i++){
			cells[i].moveRight();
			}
		}

	public void moveLeft() {
		for(int i=0;i<cells.length;i++){
			cells[i].moveLeft();
			}
		}

	public void softDrop() {
		for(int i=0;i<cells.length;i++){
			cells[i].softDrop();
		}
		
	}
	
		
	}

//extends：是，T型方块是Tetromino（4格方块）
class T extends Tetromino{
	public T(){
		cells[0]=new Cell(0,4,Tetris.T);
		cells[1]=new Cell(0,3,Tetris.T);
		cells[2]=new Cell(0,5,Tetris.T);
		cells[3]=new Cell(1,4,Tetris.T);
		states=new State[4];//存储旋转状态
		states[0]=new State(0,0,0,-1,0,1,1,0);
		states[1]=new State(0,0,-1,0,1,0,0,-1);
		states[2]=new State(0,0,0,1,0,-1,-1,0);
		states[3]=new State(0,0,1,0,-1,0,0,1);
		}
}
class S extends Tetromino{
	public S(){
		cells[0]=new Cell(0,4,Tetris.S);
		cells[1]=new Cell(0,5,Tetris.S);
		cells[2]=new Cell(1,3,Tetris.S);
		cells[3]=new Cell(1,4,Tetris.S);
		states=new State[2];//存储旋转状态
		states[0]=new State(0,0,0,-1,-1,0,-1,1);
		states[1]=new State(0,0,-1,0,0,1,1,1);
		}
	
}
class J extends Tetromino{
	public J(){
		cells[0]=new Cell(0,4,Tetris.J);
		cells[1]=new Cell(0,3,Tetris.J);
		cells[2]=new Cell(0,5,Tetris.J);
		cells[3]=new Cell(1,5,Tetris.J);
		
		states=new State[]{
			new State(0,0,0,-1,0,1,1,1),
			new State(0,0,-1,0,1,0,1,-1),
			new State(0,0,0,1,0,-1,-1,-1),
			new State(0,0,1,0,-1,0,-1,1)
			};
		}
	
}
class I extends Tetromino{
	public I(){
		cells[0]=new Cell(0,4,Tetris.I);
		cells[1]=new Cell(0,3,Tetris.I);
		cells[2]=new Cell(0,5,Tetris.I);
		cells[3]=new Cell(0,6,Tetris.I);
		states=new State[]{
				new State(0,0,0,-1,0,1,0,2),
				new State(0,0,-1,0,1,0,2,0)
				};
		}
	
	
}
class O extends Tetromino{
	public O(){
		cells[0]=new Cell(0,4,Tetris.O);
		cells[1]=new Cell(0,5,Tetris.O);
		cells[2]=new Cell(1,4,Tetris.O);
		cells[3]=new Cell(1,5,Tetris.O);
		
	 states=new State[]{new State(0,0,0,1,1,0,1,1),new State(0,0,0,1,1,0,1,1)};}
}
class L extends Tetromino{
	public L(){
		cells[0]=new Cell(0,4,Tetris.L);
		cells[1]=new Cell(0,3,Tetris.L);
		cells[2]=new Cell(0,5,Tetris.L);
		cells[3]=new Cell(1,3,Tetris.L);
		 states=new State[]{
				new State(0,0,0,1,0,-1,-1,1),
				new State(0,0,1,0,-1,0,1,1),
            new State(0,0,0,-1,0,1,1,-1),
				new State(0,0,-1,0,1,0,-1,-1)};
	}
}
class Z extends Tetromino{
	public Z(){
		cells[0]=new Cell(1,4,Tetris.Z);
		cells[1]=new Cell(0,3,Tetris.Z);
		cells[2]=new Cell(0,4,Tetris.Z);
		cells[3]=new Cell(1,5,Tetris.Z);
		 states=new State[]{new State(0,0,-1,-1,-1,0,0,1),
				new State(0,0,-1,1,0,1,1,0)};
		}
}