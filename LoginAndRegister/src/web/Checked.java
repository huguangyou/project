package web;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Checked extends HttpServlet{
	public void service(HttpServletRequest request,HttpServletResponse response) throws
	IOException,ServletException{
		request.setCharacterEncoding("utf-8");
		response.setContentType("Image/jpeg");
		//创建一个内存映像对象(画布)
		BufferedImage img=new BufferedImage(80,30, BufferedImage.TYPE_INT_RGB);
		//2.获得画笔
		Graphics g=img.getGraphics();
		Random rd=new Random();
		//给画笔设置颜色
		//g.setColor(new Color(255,255,255));
		g.setColor(new Color(rd.nextInt(255),rd.nextInt(255),rd.nextInt(255)));
		//画个矩形
		g.fillRect(0, 0, 80, 30);
		//给笔设置随机的颜色
		g.setColor(new Color(rd.nextInt(255),rd.nextInt(255),rd.nextInt(255)));
		//Font（字体，风格，大小）
		g.setFont(new Font(null,Font.ITALIC,22));
		//自定义的方法，获取长度为4的字母数字组合的字符串
		String number=getNumber(4);
		
		/*
		 * 将number绑定到session上
		 */
		HttpSession session=request.getSession();
		session.setAttribute("number", number);
		
		g.drawString(number+"", 10, 24);
		//加一些干扰线
		for(int i=0;i<6;i++){
			g.drawLine(rd.nextInt(80), rd.nextInt(30), rd.nextInt(80), rd.nextInt(30));
		}
		OutputStream ops=response.getOutputStream();
		ImageIO.write(img, "jpeg", ops);
		ops.close();
		
	}
	//长度为size个字符，并且这些字符随机从
		//"A~Z,0~9"中选取。
		private String getNumber(int size) {
			String str = "ABCDEFGHIJKLMNOPQR" +
					"STUVWXYZ0123456789";
			String number = "";
			Random r = new Random();
			for(int i = 0 ;i < size; i ++){
				number += str.charAt(r.nextInt(str.length()));
			}
			return number;
		}


}
