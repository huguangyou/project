package web;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.Emp;
import dao.EmpDao;
public class ActionServlet extends HttpServlet{
	public void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException{
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out=response.getWriter();
		
		String uri=request.getRequestURI();	
		String action=uri.substring(uri.lastIndexOf("/")+1,uri.lastIndexOf(".do"));
		
		/**
		 * 对客户端发送的不同请求分别做相对应的处理
		 */
		if(action.equals("login_check")){
			/*session验证*/
			HttpSession session=request.getSession();
			//设置session超时时间20秒
			session.setMaxInactiveInterval(20);
			//先进行验证码比较
			String number=request.getParameter("number");
			String number1=(String)session.getAttribute("number");
			if(!number.equalsIgnoreCase(number1)){
					//验证码不正确，不用比较用户名和密码 
					//了，跳转到登录页面，并提示用户
					request.setAttribute("number_error",
							"验证码错误");
					request.getRequestDispatcher("login.jsp")
					.forward(request, response);
					return;
			}
			
			String name=request.getParameter("name");
			String password=request.getParameter("password");
		    
			EmpDao empDao=new EmpDao();
			try {
			  Emp emp = empDao.find(name);
			if(emp!=null && password.equals(emp.getPassword())){
				//登录成功，绑订一些数据到session对象上,再重定向
				session.setAttribute("emp", emp);
				response.sendRedirect("main.jsp");
			}else{
				/*转发*/
				request.setAttribute("error", "用户名或者密码不正确");
				request.getRequestDispatcher("login.jsp").forward(request, response);
			}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new ServletException(e);
			}
		}else if(action.equals("registCheck_name")){
			String name = 
					request.getParameter("username");
			EmpDao empDao=new EmpDao();
			try {
				Emp emp = empDao.find(name);
				if(emp!=null && name.trim().equals(emp.getName())){		
					out.println("用户名被占用");
				}else{
					out.println("可以使用");
				}
				
			} catch (SQLException e) {
				e.printStackTrace();
				throw new ServletException(e);
			}
				
		}else if(action.equals("")){}
		out.close();
		
	}
}
