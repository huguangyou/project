package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * 对数据库的进行操作的类
 * @author Administrator
 *
 */
public class EmpDao {
	Connection conn;
	PreparedStatement ps;
	ResultSet rs;
	public List<Emp> findAll() throws Exception{
		List<Emp>emps=new ArrayList<Emp>();
		//Emp emp=null;
		conn=null;
		ps=null;
		rs=null;
	try {
		conn = DBUtil.getConnection();
		String sql="select * from user_1";
		ps=conn.prepareStatement(sql);
		rs=ps.executeQuery();
		while(rs.next()){
			Emp emp=new Emp();
			emp.setId(rs.getInt("id"));
			emp.setName(rs.getString("name"));
			emp.setAge(rs.getInt("age"));
			emp.setSex(rs.getInt("sex"));
			emps.add(emp);
		}
	} catch (SQLException e) {
		e.printStackTrace();
		throw e;
	}finally{
		DBUtil.close(conn,ps,rs);
	}
	return emps;
	}
	/**
	 * 添加员工信息
	 * @param emp
	 * @throws SQLException
	 */
	public void insert(Emp emp) throws SQLException{
		conn=null;
		ps=null;
		try {
			conn = DBUtil.getConnection();
			String sql="insert into user_1 values(?,?,?,?,?)";
			ps=conn.prepareStatement(sql);
			ps.setInt(1, emp.getId());
			ps.setString(2, emp.getName());
			ps.setString(3, emp.getPassword());
			ps.setInt(4, emp.getAge());
			ps.setInt(5,emp.getSex());
			ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		}finally{
			DBUtil.close(conn,ps,null);
		}
	}
	/**
	 *  删除员工信息
	 * @param emp
	 * @throws SQLException
	 */
	public void delete(Emp emp) throws SQLException{
		conn=null;
		ps=null;
		try {
			conn=DBUtil.getConnection();
			String sql="delete from user_1 where id=?";
			ps=conn.prepareStatement(sql);
			ps.setInt(1, emp.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		}finally{
			DBUtil.close(conn,ps,null);
		}
	}
	public Emp find(String name) throws SQLException{
		Emp emp=null;
		conn=null;
		ps=null;
		rs=null;
		try{
			conn=DBUtil.getConnection();
			String sql="select password,name from user_1 where name=?";
			ps=conn.prepareStatement(sql);
			ps.setString(1,name);
			ps.executeQuery();
			rs=ps.getResultSet();
			while(rs.next()){
				emp=new Emp();
				emp.setName(rs.getString("name"));
				System.out.println(emp.getName());
				emp.setPassword(rs.getString("password"));
			}
		}catch(SQLException e){
			e.printStackTrace();
			throw e;
		}finally{
			DBUtil.close(conn, ps, rs);
		}
		return emp;
	}
	
}
