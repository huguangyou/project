package tag;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class DateTag extends SimpleTagSupport{
	private String pattern;
	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public void doTag() throws IOException{
		//通过SimpleTagSupport的方法获得pageContext
		PageContext ctx=(PageContext) getJspContext();
		//pageContext提供了找到其它所有隐含
		JspWriter out = ctx.getOut();
		Date date=new Date();
		SimpleDateFormat sdf=new SimpleDateFormat(pattern);
		out.println(sdf.format(date));
	}
}
