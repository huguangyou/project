function checkedName(){
	//1.获取Ajax对象
	var xhr=getXhr();
	//2.发送异步请求
	xhr.open("post", 'registCheck_name.do',true);
	xhr.setRequestHeader('content-type',
			'application/x-www-form-urlencoded');
	//3.绑定一个事件处理函数
	xhr.onreadystatechange=function(){
		if(xhr.readyState==4&&xhr.status==200){
				//获取所有文本
				var txt=xhr.responseText;
				$("username").parentNode.getElementsByTagName('span')[0].innerHTML=txt;
		}
	};
	xhr.send("username="+$F('username'));
	
}
//获得Ajax对象
function getXhr(){
	var xhr=null;
	if(window.XMLHttpRequest){
		xhr=new XMLHttpRequest();
	}else{
		xhr=new ActiveXObject("MicroSoft.XMLHttp");
	}
	return xhr;
}
function $(name){
	return document.getElementsByName(name)[0];
}
function $F(name){
		return $(name).value;
}