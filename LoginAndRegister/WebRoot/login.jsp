<%@page contentType="text/html;charset=utf-8" pageEncoding="UTF-8"%>
<%@page import="java.util.*,java.text.*" %>
<%--自定义的标签 --%>
<%@taglib uri="/test" prefix="c1" %>
<html>
	<head>
		<title>login</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link rel="stylesheet" type="text/css"href="css/style.css" />
	</head>
	<body>
		<div id="wrap">
			<div id="top_content">
					<div id="header">
						<div id="rightheader">
							<p>
							<%--自定义的标签 --%>
							<c1:date pattern="yyyy-MM-dd"/><br />
							</p>
						</div>
						<div id="topheader">
							<h1 id="title">
								<a href="#">main</a>
							</h1>
						</div>
						<div id="navigation">
						</div>
					</div>
				<div id="content">
					<p id="whereami">
					</p>
					<h1>
						login
					</h1>
					<form action="login_check.do" method="post">
						<table cellpadding="0" cellspacing="0" border="0"
							class="form_table">
							<tr>
								<td valign="middle" align="right">
									用户:
								</td>
								<td valign="middle" align="left">
									<input type="text" class="inputgri" name="name" />
								</td>
								<td>
								<%
									String msg = (String)request.getAttribute("error");
				 				%>
				                <span style="color:red;"><%=(msg == null ? "" : msg)%></span>
								</td>
							</tr>
							<tr>
								<td valign="middle" align="right">
									密码:
								</td>
								<td valign="middle" align="left">
									<input type="password" class="inputgri" name="password" />
								</td>
							</tr>
						</table>
						<p>
							<input type="submit" class="button" value="登录 &raquo;" />
							<input type="text" style="width: 70px;height: 32px" name="number"/>
							<img id="img1" src="check" border="1px"/>
							<a href="javascript:;" 
		onclick="document.getElementById('img1').src='check?' + Math.random();">看不清，换一个</a>
		<% String msg2 = 
					(String)request.getAttribute("number_error");%>
		<%=(msg2 == null ? "" : msg2) %>
						</p>
					</form>
				</div>
			</div>
			<div id="footer">
				<div id="footer_bg">
					ABC@126.com
				</div>
			</div>
		</div>
	</body>
</html>
